<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv;

use PHPUnit_Framework_TestCase;

// https://github.com/wdalmut/perceptron-sigmoid/blob/master/src/Perceptron.php

class HelperTest extends PHPUnit_Framework_TestCase {
	public function testNormalize() {
		$this->assertEquals(-1, Helper::normalize(10, 10, 20));
		$this->assertEquals(1, Helper::normalize(20, 10, 20));
		$this->assertEquals(0, Helper::normalize(15, 10, 20));
		$this->assertEquals(-0.5, Helper::normalize(12.5, 10, 20));
		$this->assertEquals(1, Helper::normalize(1, 1, 1));
	}

	/**
	 * @expectedException InvalidArgumentException
	 * @expectedExceptionMessage First parameter must be 2
	 */
	public function testNormalizeEquals() {
		Helper::normalize(1, 2, 2);
	}

}
