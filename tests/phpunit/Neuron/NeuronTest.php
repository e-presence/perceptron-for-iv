<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Neuron;

use \stdClass;
use PHPUnit_Framework_TestCase;

class NeuronTest extends PHPUnit_Framework_TestCase {
	private $n;

	public function setUp() {
		$this->n = new Neuron(
			2,
			array(
				array(10, 20),
				array(30, 40),
			)
		);
	}

	public function testBasicInstance() {
		$this->assertInstanceOf('EPresence\PerceptronForIv\Neuron\Neuron', $this->n);
	}

	/**
	 * @param mixed $a
	 * @param mixed $b
	 * @param mixed $c
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidNeuronParametersTypeError
	 */
	public function testInvalidTypeParameters($a, $b, $c) {
		$p = new Neuron($a, $b, $c);
	}

	public function invalidNeuronParametersTypeError() {
		return array(
			array(1, null, 1),
			array(1, 123, 1),
			array(1, 'intervals', 1),
			array(1, new stdClass(), 1),
		);
	}

	/**
	 * @param mixed $a
	 * @param mixed $b
	 * @param mixed $c
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider invalidNeuronParameters
	 */
	public function testInvalidParameters($a, $b, $c) {
		$p = new Neuron($a, $b, $c);
	}

	public function invalidNeuronParameters() {
		return array(
			array(-1, array(array(10, 20), array(10, 20)), 1),
			array(0, array(array(10, 20), array(10, 20)), 1),
			array(2, array(), 1),
			array(2, array(1, 1), 1),
			array(2, array(array(3 => 10, 20), array(10, 20)), 1),
			array(2, array(array(10, 3 => 20), array(10, 20)), 1),
			array(2, array(array(10, 20), array(3 => 10, 20)), 1),
			array(2, array(array(10, 20), array(10, 3 => 20)), 1),
			array(2, array(array(10), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10)), 1),
			array(2, array(array(10, 'test'), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 'test')), 1),
			array(2, array(array(10, 10), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 10)), 1),
			array(2, array(array(10, 5), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 5)), 1),
		);
	}

	public function testGetInputIntervals() {
		$ii = $this->n->getInputIntervals();
		$this->assertInternalType('array', $ii);
		$this->assertCount(2, $ii);
		$this->assertCount(2, $ii[0]);
		$this->assertCount(2, $ii[1]);
		$this->assertEquals(10, $ii[0][0]);
		$this->assertEquals(20, $ii[0][1]);
		$this->assertEquals(30, $ii[1][0]);
		$this->assertEquals(40, $ii[1][1]);
	}

	public function testGetWeights() {
		$w = $this->n->getWeights();
		$this->assertInternalType('array', $w);
		$this->assertCount(3, $w);
		$this->assertGreaterThanOrEqual(-1, $w[0]);
		$this->assertGreaterThanOrEqual(-1, $w[1]);
		$this->assertLessThanOrEqual(1, $w[0]);
		$this->assertLessThanOrEqual(1, $w[1]);
	}

	public function testGetInputCount() {
		$this->assertEquals(2, $this->n->getInputCount());
	}

	/**
	 * @param mixed $w
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidTypeWeightsData
	 */
	public function testSetInvalidTypeWeigths($w) {
		$this->n->setWeights($w);
	}

	public function invalidTypeWeightsData() {
		return array(
			array('weights'),
			array(null),
			array(123),
		);
	}

	/**
	 * @param mixed $w
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider invalidWeightsData
	 */
	public function testSetInvalidWeigths($w) {
		$this->n->setWeights($w);
	}

	public function invalidWeightsData() {
		return array(
			array(array(1)),
			array(array(1, 2)),
			array(array(1, 3 => 1)),
		);
	}

	public function testSetWeight() {
		$this->n->setWeights(array(0.12, 0.34, 1));
		$w = $this->n->getWeights();
		$this->assertInternalType('array', $w);
		$this->assertCount(3, $w);
		$this->assertEquals(0.12, $w[0]);
		$this->assertEquals(0.34, $w[1]);
	}

	/**
	 * @param mixed $i
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidTypeInputsData
	 */
	public function testSetInvalidTypeInputs($i) {
		$this->n->process($i);
	}

	public function invalidTypeInputsData() {
		return array(
			array('inputs'),
			array(null),
			array(123),
		);
	}

	/**
	 * @param mixed $i
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider invalidInputsData
	 */
	public function testSetInvalidInputs($i) {
		$this->n->process($i);
	}

	public function invalidInputsData() {
		return array(
			array(array()),
			array(array(1)),
			array(array(1, 2, 3)),
			array(array(1, 35)),
			array(array(21, 35)),
			array(array(15, 1)),
			array(array(15, 41)),
			array(array(0 => 15, 2 => 35)),
		);
	}

	public function testProcess() {
		$this->n->process(array(15, 35));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testRealSaveAndLoad() {
		$path = __DIR__ . '/../../../mock/test.neuron';
		$p = new Neuron(3, array(array(1, 2), array(3, 4), array(5, 6)));
		$p->save($path);
		$p2 = Neuron::load($path);
		$this->assertEquals($p, $p2);
	}

}
