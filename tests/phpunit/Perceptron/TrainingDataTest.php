<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-resence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

use \stdClass;
use PHPUnit_Framework_TestCase;

class TrainingDataTest extends PHPUnit_Framework_TestCase {
	public function testBasicInstance() {
		$td = new TrainingData(array(1, 2), 3);
		$this->assertEquals(array(1, 2), $td->input);
		$this->assertEquals(3, $td->output);
	}

	/**
	 * @param mixed $p
	 *
	 * @dataProvider invalidParameters
	 *
	 * @expectedException TypeError
	 */
	public function testInvalidParameters($p) {
		$td = new TrainingData($p, 1);
	}

	public function invalidParameters() {
		return array(
			array(null),
			array(1),
			array('parameter'),
			array(new stdClass()),
		);
	}
}
