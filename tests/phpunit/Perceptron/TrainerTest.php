<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

use \stdClass;
use PHPUnit_Framework_TestCase;

class TrainerTest extends PHPUnit_Framework_TestCase {
	private $p;

	public function setUp() {
		$this->p = new Perceptron(
			2,
			array(
				array(10, 20),
				array(10, 20),
			)
		);
		$this->t = new Trainer($this->p);
	}

	public function testBasicInstance() {
		$this->assertInstanceOf('EPresence\PerceptronForIv\Perceptron\Trainer', $this->t);
	}

	/**
	 * @param mixed $set
	 * @param mixed $expected
	 *
	 * @dataProvider dataForTestSetGetUnchangedWeights
	 */
	public function testSetGetUnchangedWeights($set, $expected) {
		$this->t->setMinUnchangedWeigths($set);
		$this->assertEquals($expected, $this->t->getMinUnchangedWeigths());
	}

	public function dataForTestSetGetUnchangedWeights() {
		return array(
			array(3, 3),
			array(15, 15),
			array(3.21, 3),
			array(0, Trainer::DEFAULT_MIN_UNCHANGED_WEIGHTS),
			array(-1, Trainer::DEFAULT_MIN_UNCHANGED_WEIGHTS),
			array('test', Trainer::DEFAULT_MIN_UNCHANGED_WEIGHTS),
			array(array(), Trainer::DEFAULT_MIN_UNCHANGED_WEIGHTS),
			array(array(1, 2, 3), 1),
		);
	}

	/**
	 * @param mixed $p
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidPerceptronData
	 */
	public function testInvalidSetPerceptron($p) {
		$this->t->setPerceptron($p);
	}

	public function invalidPerceptronData() {
		return array(
			array(array(1)),
			array(array(1, 2, 3)),
			array(null),
			array(123),
			array('perceptron'),
			array(new stdClass()),
		);
	}

	public function testGetPerceptron() {
		$p = $this->t->getPerceptron();
		$this->assertInstanceOf('EPresence\PerceptronForIv\Perceptron\Perceptron', $p);
	}

	public function testSetGetMaxEpochs() {
		$m = $this->t->getMaxEpochs();
		$this->assertEquals(Trainer::DEFAULT_MAX_EPOCHS, $m);
		$this->t->setMaxEpochs(3000);
		$m = $this->t->getMaxEpochs();
		$this->assertEquals(3000, $m);
		$this->t->setMaxEpochs(-3000);
		$m = $this->t->getMaxEpochs();
		$this->assertEquals(Trainer::DEFAULT_MAX_EPOCHS, $m);
	}

	public function testGetEmptyTrainingDataSet() {
		$td = $this->t->getTrainingDataSet();
		$this->assertInternalType('array', $td);
		$this->assertCount(0, $td);
	}

	/**
	 * @param mixed $td
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidTypeAddTrainingDataSet
	 */
	public function testInvalidTypeAddTrainingDataSet($td) {
		$this->t->addTrainingData(new TrainingData($td, 0));
	}

	public function InvalidTypeAddTrainingDataSet() {
		return array(
			array(null),
			array(123),
			array('training_data'),
			array(new stdClass()),
		);
	}

	/**
	 * @param mixed $td
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider InvalidArgumentAddTrainingDataSet
	 */
	public function testInvalidArgumentAddTrainingDataSet($td) {
		$this->t->addTrainingData($td);
	}

	public function InvalidArgumentAddTrainingDataSet() {

		return array(
			array(new TrainingData(array(), 1)),
			array(new TrainingData(array(1), 1)),
			array(new TrainingData(array(1, 2, 3), 1)),
			array(new TrainingData(array(10, 1), 1)),
			array(new TrainingData(array(10, 21), 1)),
			array(new TrainingData(array(1, 10), 1)),
			array(new TrainingData(array(31, 10), 1)),
			array(new TrainingData(array(10, 10), 'output')),
			array(new TrainingData(array(10, 10), 4)),
			array(new TrainingData(array(10, 10), '0')),
			array(new TrainingData(array(10, 10), null)),
			array(new TrainingData(array(10, 10), -2)),
		);
	}

	public function testSetGetTrainingDataSet() {
		$this->t->addTrainingData(new TrainingData(array(10, 11), -1));
		$this->t->addTrainingData(new TrainingData(array(12, 13), 1));
		$this->t->addTrainingData(new TrainingData(array(14, 15), 1));
		$td = $this->t->getTrainingDataSet();
		$this->assertInternalType('array', $td);
		$this->assertCount(3, $td);
		$this->t->addTrainingData(new TrainingData(array(14, 15), 1));
		$td = $this->t->getTrainingDataSet();
		$this->assertCount(3, $td);
	}

	public function testTrainAND() {
		$p = new Perceptron(2, array(array(-1, 1), array(-1, 1)));
		$t = new Trainer($p, 2000);
		$t->addTrainingData(new TrainingData(array(-1, -1), -1));
		$t->addTrainingData(new TrainingData(array(-1, 1), -1));
		$t->addTrainingData(new TrainingData(array(1, -1), -1));
		$t->addTrainingData(new TrainingData(array(1, 1), 1));
		$t->train();
		$this->assertEquals(-1, round($p->process(array(-1, -1))));
		$this->assertEquals(-1, round($p->process(array(-1, 1))));
		$this->assertEquals(-1, round($p->process(array(1, -1))));
		$this->assertEquals(1, round($p->process(array(1, 1))));

	}

	public function testTrainOR() {
		$p = new Perceptron(2, array(array(-1, 1), array(-1, 1)));
		$t = new Trainer($p);
		$t->addTrainingData(new TrainingData(array(-1, -1), -1));
		$t->addTrainingData(new TrainingData(array(-1, 1), 1));
		$t->addTrainingData(new TrainingData(array(1, -1), 1));
		$t->addTrainingData(new TrainingData(array(1, 1), 1));
		$t->train();
		$this->assertEquals(-1, round($p->process(array(-1, -1))));
		$this->assertEquals(1, round($p->process(array(-1, 1))));
		$this->assertEquals(1, round($p->process(array(1, -1))));
		$this->assertEquals(1, round($p->process(array(1, 1))));
	}

	public function testTrainNAND() {
		$p = new Perceptron(2, array(array(-1, 1), array(-1, 1)));
		$t = new Trainer($p);
		$t->addTrainingData(new TrainingData(array(-1, -1), 1));
		$t->addTrainingData(new TrainingData(array(-1, 1), 1));
		$t->addTrainingData(new TrainingData(array(1, -1), 1));
		$t->addTrainingData(new TrainingData(array(1, 1), -1));
		$t->train();
		$this->assertEquals(1, round($p->process(array(-1, -1))));
		$this->assertEquals(1, round($p->process(array(-1, 1))));
		$this->assertEquals(1, round($p->process(array(1, -1))));
		$this->assertEquals(-1, round($p->process(array(1, 1))));
	}

	public function testTrainNOR() {
		$p = new Perceptron(2, array(array(-1, 1), array(-1, 1)));
		$t = new Trainer($p);
		$t->addTrainingData(new TrainingData(array(-1, -1), 1));
		$t->addTrainingData(new TrainingData(array(-1, 1), -1));
		$t->addTrainingData(new TrainingData(array(1, -1), -1));
		$t->addTrainingData(new TrainingData(array(1, 1), -1));
		$t->train();
		$this->assertEquals(1, round($p->process(array(-1, -1))));
		$this->assertEquals(-1, round($p->process(array(-1, 1))));
		$this->assertEquals(-1, round($p->process(array(1, -1))));
		$this->assertEquals(-1, round($p->process(array(1, 1))));
	}

	public function testRain() {
		$p = new Perceptron(2, array(array(0, 100), array(0, 100)));
		$t = new Trainer($p);
		$t->addTrainingData(new TrainingData(array(0, 0), 1));
		$t->addTrainingData(new TrainingData(array(100, 100), -1));
		$t->addTrainingData(new TrainingData(array(33, 66), -1));
		$t->addTrainingData(new TrainingData(array(66, 33), -1));
		$t->addTrainingData(new TrainingData(array(10, 10), 1));
		$t->addTrainingData(new TrainingData(array(0, 33), 1));
		$t->addTrainingData(new TrainingData(array(33, 0), 1));
		$t->train();
		$this->assertEquals(1, round($p->process(array(50, 0))));
	}

	public function testPattern() {
		$p = new Perceptron(6, array(
			array(0, 1),
			array(0, 1),
			array(0, 1),
			array(0, 1),
			array(0, 1),
			array(0, 1),
		));
		$t = new Trainer($p);

		$t->addTrainingData(new TrainingData(array(1, 1, 1, 1, 1, 1), 1));
		$t->addTrainingData(new TrainingData(array(0, 1, 1, 1, 1, 1), 1));
		$t->addTrainingData(new TrainingData(array(1, 1, 1, 1, 1, 0), 1));

		$t->addTrainingData(new TrainingData(array(0, 0, 0, 0, 0, 0), -1));
		$t->addTrainingData(new TrainingData(array(1, 0, 0, 0, 0, 0), -1));
		$t->addTrainingData(new TrainingData(array(0, 0, 0, 0, 0, 1), -1));
		$t->train();

		$this->assertEquals(1, round($p->process(array(1, 1, 0, 1, 1, 1))));
		$this->assertEquals(1, round($p->process(array(1, 1, 1, 0, 1, 1))));
		$this->assertEquals(1, round($p->process(array(1, 1, 1, 1, 0, 1))));
		$this->assertEquals(1, round($p->process(array(1, 0, 1, 1, 1, 1))));

		$this->assertEquals(-1, round($p->process(array(0, 0, 1, 1, 0, 0))));
		$this->assertEquals(-1, round($p->process(array(1, 0, 0, 0, 0, 1))));
		$this->assertEquals(-1, round($p->process(array(0, 0, 1, 0, 0, 0))));
		$this->assertEquals(-1, round($p->process(array(0, 0, 0, 1, 0, 0))));
	}

	public function testSomethingElse() {
		$p = new Perceptron(3, array(
			array(1, 5),
			array(1, 5),
			array(1, 5),
		));
		$t = new Trainer($p);

		$t->addTrainingData(new TrainingData(array(5, 5, 5), 1));
		$t->addTrainingData(new TrainingData(array(3, 5, 5), 1));
		$t->addTrainingData(new TrainingData(array(5, 4, 4), 1));
		$t->addTrainingData(new TrainingData(array(3, 3, 3), -1));
		$t->addTrainingData(new TrainingData(array(2, 2, 2), -1));
		$t->addTrainingData(new TrainingData(array(1, 1, 1), -1));

		$t->train();

		$this->assertEquals(1, round($p->process(array(5, 4, 4))));
		$this->assertEquals(1, round($p->process(array(5, 4, 3))));
		$this->assertEquals(0, round($p->process(array(5, 4, 2))));
		$this->assertEquals(-1, round($p->process(array(5, 4, 1))));
		$this->assertEquals(-1, round($p->process(array(1, 4, 2))));
		$this->assertEquals(-1, round($p->process(array(5, 3, 1))));
	}

}
