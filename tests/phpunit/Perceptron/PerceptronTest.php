<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

use \stdClass;
use PHPUnit_Framework_TestCase;

class PerceptronTest extends PHPUnit_Framework_TestCase {
	private $p;

	public function setUp() {
		$this->p = new Perceptron(
			2,
			array(
				array(10, 20),
				array(30, 40),
			)
		);
	}

	public function testBasicInstance() {
		$this->assertInstanceOf('EPresence\PerceptronForIv\Perceptron\Perceptron', $this->p);
	}

	/**
	 * @param mixed $a
	 * @param mixed $b
	 * @param mixed $c
	 *
	 * @expectedException TypeError
	 *
	 * @dataProvider invalidPerceptronParametersTypeError
	 */
	public function testInvalidTypeParameters($a, $b, $c) {
		$p = new Perceptron($a, $b, $c);
	}

	public function invalidPerceptronParametersTypeError() {
		return array(
			array(1, null, 1),
			array(1, 123, 1),
			array(1, 'intervals', 1),
			array(1, new stdClass(), 1),
		);
	}

	/**
	 * @param mixed $a
	 * @param mixed $b
	 * @param mixed $c
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider invalidPerceptronParameters
	 */
	public function testInvalidParameters($a, $b, $c) {
		$p = new Perceptron($a, $b, $c);
	}

	public function invalidPerceptronParameters() {
		return array(
			array(-1, array(array(10, 20), array(10, 20)), 1),
			array(0, array(array(10, 20), array(10, 20)), 1),
			array(2, array(), 1),
			array(2, array(1, 1), 1),
			array(2, array(array(3 => 10, 20), array(10, 20)), 1),
			array(2, array(array(10, 3 => 20), array(10, 20)), 1),
			array(2, array(array(10, 20), array(3 => 10, 20)), 1),
			array(2, array(array(10, 20), array(10, 3 => 20)), 1),
			array(2, array(array(10), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10)), 1),
			array(2, array(array(10, 'test'), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 'test')), 1),
			array(2, array(array(10, 10), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 10)), 1),
			array(2, array(array(10, 5), array(10, 20)), 1),
			array(2, array(array(10, 20), array(10, 5)), 1),
		);
	}

	/**
	 * @param mixed $t
	 * @param mixed $t
	 *
	 * @expectedException InvalidArgumentException
	 *
	 * @dataProvider invalidTrainData
	 */
	public function testSetInvalidtrain($t, $o) {
		$this->p->train(new TrainingData($t, $o));
	}

	public function invalidTrainData() {
		return array(
			array(array(), 1),
			array(array(1), 1),
			array(array(1, 2, 3), 1),
			array(array(1, 35), 1),
			array(array(21, 35), 1),
			array(array(15, 1), 1),
			array(array(15, 41), 1),
			array(array(15, 35), 'output'),
			array(array(15, 35), -2),
			array(array(15, 35), 3),
			array(array(15, 35), 1.23),
			array(array(15, 35), array()),
		);
	}

	public function testTrain() {
		$this->p->train(new TrainingData(array(15, 35), 1));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testRealSaveAndLoad() {
		$path = __DIR__ . '/../../../mock/test.perceptron';
		$p = new Perceptron(3, array(array(1, 2), array(3, 4), array(5, 6)));
		$p->save($path);
		$p2 = Perceptron::load($path);
		$this->assertEquals($p, $p2);
	}

}
