<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Filesystem;

use PHPUnit_Framework_TestCase;

class Dummy {
	use SaveAndLoadTrait;

	private $data = 'Test data';
}

class SaveAndLoadTraitTest extends PHPUnit_Framework_TestCase {
	private $data;
	private $path = '/tmp/test.object';

	public function setUp() {
		$this->data = new Dummy();
	}

	public function testBasicInstance() {
		$this->assertTrue(is_callable(array($this->data, 'save')));
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testSave() {
		require_once __DIR__ . '/../../../mock/File.mock.php';
		$mock = $this->getMockBuilder('EPresence\PerceptronForIv\Filesystem\File')
			->setMethods(array('isWritable', 'put'))
			->getMock();
		$mock->expects($this->once())
			->method('isWritable')
			->with(str_replace(basename($this->path), '', $this->path))
			->will($this->returnValue(true));
		$mock->expects($this->any())
			->method('put')
			->with($this->path, serialize($this->data))
			->will($this->returnValue(true));
		\EPresence\PerceptronForIv\Filesystem\File::setStaticExpectations($mock);

		$this->data->save($this->path);
	}

	/**
	 * @runInSeparateProcess
	 * @expectedException Exception
	 */
	public function testSaveNotWritablePath() {
		require_once __DIR__ . '/../../../mock/File.mock.php';
		$mock = $this->getMockBuilder('EPresence\PerceptronForIv\Filesystem\File')
			->setMethods(array('isWritable'))
			->getMock();
		$mock->expects($this->once())
			->method('isWritable')
			->with(str_replace(basename($this->path), '', $this->path))
			->will($this->returnValue(false));
		\EPresence\PerceptronForIv\Filesystem\File::setStaticExpectations($mock);

		$this->data->save($this->path);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testLoad() {
		require_once __DIR__ . '/../../../mock/File.mock.php';
		$mock = $this->getMockBuilder('EPresence\PerceptronForIv\Filesystem\File')
			->setMethods(array('exists', 'get'))
			->getMock();
		$mock->expects($this->once())
			->method('exists')
			->with($this->path)
			->will($this->returnValue(true));
		$mock->expects($this->any())
			->method('get')
			->with($this->path)
			->will($this->returnValue(serialize($this->data)));
		\EPresence\PerceptronForIv\Filesystem\File::setStaticExpectations($mock);

		$p = Dummy::load($this->path);
		$this->assertEquals($p, $this->data);
	}

	/**
	 * @runInSeparateProcess
	 * @expectedException Exception
	 */
	public function testLoadFileNotExists() {
		require_once __DIR__ . '/../../../mock/File.mock.php';
		$mock = $this->getMockBuilder('EPresence\PerceptronForIv\Filesystem\File')
			->setMethods(array('exists', 'get'))
			->getMock();
		$mock->expects($this->once())
			->method('exists')
			->with($this->path)
			->will($this->returnValue(false));
		\EPresence\PerceptronForIv\Filesystem\File::setStaticExpectations($mock);

		$d = Dummy::load($this->path);
	}

	/**
	 * @runInSeparateProcess
	 * @expectedException InvalidArgumentException
	 */
	public function testLoadInvalidData() {
		require_once __DIR__ . '/../../../mock/File.mock.php';
		$mock = $this->getMockBuilder('EPresence\PerceptronForIv\Filesystem\File')
			->setMethods(array('exists', 'get'))
			->getMock();
		$mock->expects($this->once())
			->method('exists')
			->with($this->path)
			->will($this->returnValue(true));
		$mock->expects($this->any())
			->method('get')
			->with($this->path)
			->will($this->returnValue(serialize('dummy data')));
		\EPresence\PerceptronForIv\Filesystem\File::setStaticExpectations($mock);

		$d = Dummy::load($this->path);
	}

	/**
	 * @runInSeparateProcess
	 */
	public function testRealSaveAndLoad() {
		$this->path = __DIR__ . '/../../../mock/test.object';
		$d = new Dummy();
		$d->save($this->path);
		$d2 = Dummy::load($this->path);
		$this->assertEquals($d, $d2);
	}

}
