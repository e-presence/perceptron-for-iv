<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
$loader = require __DIR__ . '/../../vendor/autoload.php';

$loader->addPsr4('EPresence\\PerceptronForIv\\', __DIR__);
