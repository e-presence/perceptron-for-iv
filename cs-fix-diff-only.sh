#!/bin/bash
# @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
# @copyright Copyright (c) 2016-2017, e-presence, http://e-presence.hu
php-cs-fixer fix src --fixers=-pre_increment,-concat_without_spaces,-remove_leading_slash_use,-braces,-indentation --diff
php-cs-fixer fix tests/phpunit --fixers=-pre_increment,-concat_without_spaces,-remove_leading_slash_use,-braces,-indentation --diff
php-cs-fixer fix examples --fixers=-pre_increment,-concat_without_spaces,-remove_leading_slash_use,-braces,-indentation --diff