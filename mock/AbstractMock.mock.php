<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */

namespace EPresence\PerceptronForIv;

abstract class AbstractMock {

	protected static $mocks = array();

	/**
	 * @param mixed $mock
	 *
	 * @return void
	 */
	public static function setStaticExpectations($mock) {
		self::$mocks[get_called_class()] = $mock;
	}

	/**
	 * Static calls are passed to self::$mock
	 *
	 * @param string $name
	 * @param array $args
	 *
	 * @return mixed
	 */
	public static function __callStatic($name, $args) {

		return call_user_func_array(
			array(self::$mocks[get_called_class()], $name), $args
		);
	}
}
