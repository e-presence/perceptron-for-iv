<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */

namespace EPresence\PerceptronForIv\FileSystem;

require_once(__DIR__ . '/AbstractMock.mock.php');

use EPresence\PerceptronForIv\AbstractMock;

class File extends AbstractMock {}
