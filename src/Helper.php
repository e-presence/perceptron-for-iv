<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv;

use \InvalidArgumentException;

class Helper {
	const MIN = -1;
	const MAX = 1;
	const DEFAULT_LEARNING_RATE = 0.1;

	/**
	 * @param numeric $d
	 * @param numeric $d_min
	 * @param numeric $d_max
	 *
	 * @return numeric
	 *
	 * @throws InvalidArgumentException
	 */
	public static function normalize($d, $d_min, $d_max) {
		if ($d_max - $d_min == 0) {
			if ($d != $d_min) {
				throw new InvalidArgumentException(sprintf('First parameter must be %s', $d_min));
			} else {
				$i = $d;
			}
		} else {
			$i = static::MIN + (static::MAX - static::MIN) * ($d - $d_min) / ($d_max - $d_min);
		}

		return $i;
	}

}
