<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

class TrainingData {
	public $input;
	public $output;

	public function __construct(array $input, $output) {
		$this->input = $input;
		$this->output = $output;
	}
}
