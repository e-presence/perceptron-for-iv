<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

class Trainer {
	const DEFAULT_MAX_EPOCHS = 1000;
	const DEFAULT_MIN_UNCHANGED_WEIGHTS = 5;

	private $perceptron;
	private $maxEpochs;
	private $minUnchangedWeigths;
	private $maxError; // not in use yet
	private $trainingDataSet;

	/**
	 * @param Perceptron $perceptron
	 * @param int $max_epochs
	 * @param int $min_unchanged_weights
	 *
	 * @return Trainer
	 */
	public function __construct(
			Perceptron $perceptron,
			$max_epochs = self::DEFAULT_MAX_EPOCHS,
			$min_unchanged_weights = self::DEFAULT_MIN_UNCHANGED_WEIGHTS
	) {
		$this->setPerceptron($perceptron);
		$this->setMaxEpochs($max_epochs);
		$this->setMinUnchangedWeigths($min_unchanged_weights);
		$this->trainingDataSet = array();
	}

	/**
	 * @param Perceptron $perceptron
	 */
	public function setPerceptron(Perceptron $perceptron) {
		$this->perceptron = $perceptron;
	}

	/**
	 * @return Perceptron
	 */
	public function getPerceptron() {

		return $this->perceptron;
	}

	/**
	 * @param int $max_epochs
	 */
	public function setMaxEpochs($max_epochs) {
		$max_epochs = (int) $max_epochs;
		if ($max_epochs <= 0) {
			$max_epochs = self::DEFAULT_MAX_EPOCHS;
		}
		$this->maxEpochs = $max_epochs;
	}

	/**
	 * @return int
	 */
	public function getMaxEpochs() {

		return $this->maxEpochs;
	}

	/**
	 * @param int $min_unchanged_weights
	 */
	public function setMinUnchangedWeigths($min_unchanged_weights) {
		$min_unchanged_weights = (int) $min_unchanged_weights;
		if ($min_unchanged_weights <= 0) {
			$min_unchanged_weights = self::DEFAULT_MIN_UNCHANGED_WEIGHTS;
		}
		$this->minUnchangedWeigths = $min_unchanged_weights;
	}

	/**
	 * @return int
	 */
	public function getMinUnchangedWeigths() {

		return $this->minUnchangedWeigths;
	}

	/**
	 * @param TrainingData $training_data
	 */
	public function addTrainingData(TrainingData $training_data) {
		$this->perceptron->checkInput($training_data->input);
		$this->perceptron->checkOutput($training_data->output);
		$enabled_to_add = true;
		foreach ($this->trainingDataSet as $key => $stored_training_data) {
			if ($stored_training_data == $training_data) {
				$enabled_to_add = false;
				break;
			}
		}
		if ($enabled_to_add) {
			$this->trainingDataSet[] = $training_data;
		}
	}

	/**
	 * @return array
	 */
	public function getTrainingDataSet() {

		return $this->trainingDataSet;
	}

	public function train() {
		$i = 0;
		$prev_weights = array();
		while ($i < $this->maxEpochs) {
			foreach ($this->trainingDataSet as $training_data) {
				$this->perceptron->train($training_data);
			}
			if ($this->perceptron->getIterationError() < 0.0001) {
				break;
			}
			$weights = $this->perceptron->getWeights();
			if ($prev_weights == $weights) {
				$equals++;
				if ($equals >= $this->minUnchangedWeigths) {
					break;
				}
			} else {
				$equals = 0;
				$prev_weights = $weights;
			}
			$i++;
		}
	}

}
