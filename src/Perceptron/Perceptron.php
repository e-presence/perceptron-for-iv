<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Perceptron;

use \InvalidArgumentException;
use EPresence\PerceptronForIv\Helper;
use EPresence\PerceptronForIv\Neuron\Neuron;
use EPresence\PerceptronForIv\Filesystem\SaveAndLoadTrait;

class Perceptron extends Neuron {
	use SaveAndLoadTrait;

	private $iterations;
	private $iterationError;
	private $errorSum;

	/**
	 * @param int $input_count
	 * @param array $input_intervals
	 * @param bool $use_bias
	 *
	 * @return Perceptron
	 *
	 * @throws InvalidArgumentException
	 */
	public function __construct($input_count, array $input_intervals, $use_bias = true) {
		parent::__construct($input_count, $input_intervals, $use_bias);
		$this->iterations = 0;
		$this->iterationError = 1;
		$this->errorSum = 0;
	}

	/**
	 * @return number
	 */
	public function getIterationError() {

		return $this->iterationError;
	}

	/**
	 * @param TrainingData $training_data
	 *
	 * @throws InvalidArgumentException
	 */
	public function train(TrainingData $training_data) {
		$this->checkInput($training_data->input);
		$this->checkOutput($training_data->output);

		$this->iterations++;
		$output = $this->process($training_data->input);
		$error = $training_data->output - $output;

		for ($i = 0; $i < $this->inputCount; $i++) {
			$delta = Helper::DEFAULT_LEARNING_RATE
				* $error
				* Helper::normalize(
					$training_data->input[$i],
					$this->inputIntervals[$i][0],
					$this->inputIntervals[$i][1]
				);
			$this->weights[$i] += $delta;
		}

		if ($this->useBias) {
			$this->weights[$this->inputCount] += Helper::DEFAULT_LEARNING_RATE * $error;
		}

		$this->errorSum += $error;
		if ($this->errorSum != 0) {
			$this->iterationError = 1 / abs($this->iterations * $this->errorSum);
		}
	}

}
