<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Neuron;

use \InvalidArgumentException;
use EPresence\PerceptronForIv\Helper;
use EPresence\PerceptronForIv\Filesystem\SaveAndLoadTrait;

class Neuron {
	use SaveAndLoadTrait;

	protected $inputCount;
	protected $weights;
	protected $inputIntervals;
	protected $useBias;
	private $inputs;
	private $result;
	private $output;

	/**
	 * @param int $input_count
	 * @param array $input_intervals
	 *
	 * @return Perceptron
	 *
	 * @throws InvalidArgumentException
	 */
	public function __construct($input_count, array $input_intervals, $use_bias = true) {
		$this->inputCount = (int) $input_count;
		if ($this->inputCount < 1) {
			throw new InvalidArgumentException('Invalid input count');
		}
		$this->setInputIntervals($input_intervals);
		$this->useBias = (bool) $use_bias;
		$this->initWeigths();
	}

	/**
	 * @param array $input_intervals
	 *
	 * @throws InvalidArgumentException
	 */
	public function setInputIntervals(array $input_intervals) {
		for ($i = 0; $i < $this->inputCount; $i++) {
			if (
				!isset($input_intervals[$i][0])
				|| !isset($input_intervals[$i][1])
			) {
				throw new InvalidArgumentException('Missing input interval part');
			}
			$input_intervals[$i][0] *= 1;
			$input_intervals[$i][1] *= 1;
			if ($input_intervals[$i][1] <= $input_intervals[$i][0]) {
				throw new InvalidArgumentException('Invalid input interval');
			}
			$this->inputIntervals[$i] = $input_intervals[$i];
		}
	}

	/**
	 * @return array
	 */
	public function getInputIntervals() {

		return $this->inputIntervals;
	}

	/**
	 * @return number
	 */
	public function getIterationError() {

		return $this->iterationError;
	}

	/**
	 * @return array
	 */
	public function getWeights() {

		return $this->weights;
	}

	/**
	 * @param array $weightVector
	 *
	 * @throws InvalidArgumentException
	 */
	public function setWeights(array $weights) {
		$this->checkWeights($weights);
		$this->weights = $weights;
	}

	/**
	 * @param array $input
	 *
	 * @return int
	 *
	 * @throws InvalidArgumentException
	 */
	public function process(array $input) {
		$this->checkInput($input);
		$this->result = $this->sum($input);
		$this->output = $this->activate($this->result);

		return $this->output;
	}

	/**
	 * @param array $input
	 *
	 * @throws InvalidArgumentException
	 */
	public function checkInput(array $input) {
		if (count($input) != $this->inputCount) {
			throw new InvalidArgumentException('Input count mismatch');
		}
		for ($i = 0; $i < $this->inputCount; $i++) {
			if (!isset($input[$i])) {
				throw new InvalidArgumentException('Missing input index');
			}
			if (
				$input[$i] < $this->inputIntervals[$i][0]
				|| $input[$i] > $this->inputIntervals[$i][1]
			) {
				throw new InvalidArgumentException('Input is out of interval');
			}
		}
	}

	/**
	 * @return int
	 */
	public function getInputCount() {

		return $this->inputCount;
	}

	/**
	 * @param int $output
	 *
	 * @throws InvalidArgumentException
	 */
	public function checkOutput($output) {
		if ($output !== Helper::MIN && $output !== Helper::MAX) {
			throw new InvalidArgumentException('Invalid output');
		}
	}

	private function initWeigths() {
		$input_count = $this->inputCount;
		if (is_null($this->inputCount)) {
			$weight = 1;
		} else {
			if ($this->useBias) {
				$input_count++;
			}
			$weight = 1 / $input_count;
		}
		for ($i = 0; $i < $input_count; $i++) {
			$this->weights[$i] = $weight;
		}
	}

	/**
	 * @param array $weights
	 *
	 * @throws InvalidArgumentException
	 */
	private function checkWeights(array $weights) {
		$input_count = $this->inputCount;
		if ($this->useBias) {
			$input_count++;
		}
		if (count($weights) != $input_count) {
			throw new InvalidArgumentException('Weight count mismatch');
		}
		for ($i = 0; $i < $input_count; $i++) {
			if (!isset($weights[$i])) {
				throw new InvalidArgumentException('Missing weight index');
			}
		}
	}

	/**
	 * @param array $input
	 *
	 * @return numeric
	 */
	private function sum(array $input) {
		$sum = 0;
		for ($i = 0; $i < $this->inputCount; $i++) {
			$sum += Helper::normalize($input[$i], $this->inputIntervals[$i][0], $this->inputIntervals[$i][1]) * $this->weights[$i];
		}
		if ($this->useBias) {
			$sum += $this->weights[$this->inputCount];
		}

		return $sum;
	}

	/**
	 * @param numeric $sum
	 *
	 * @return number
	 *
	 * @todo implement more functions
	 */
	private function activate($sum) {
		$sum *= 1;

		$ret_val = tanh($sum);

		return $ret_val;
	}

}
