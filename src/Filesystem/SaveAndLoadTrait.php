<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Filesystem;

use \Exception;
use \InvalidArgumentException;

trait SaveAndLoadTrait {
	/**
	 * @param string $path
	 *
	 * @throws Exception
	 */
	public function save($path) {
		$path = trim($path);
		if (!File::isWritable(str_replace(basename($path), '', $path))) {
			throw new Exception($path . ' is not writeable.');
		}
		File::put($path, serialize($this));
	}

	/**
	 * @param string $path
	 *
	 * @return object
	 *
	 * @throws Exception;
	 * @throws InvalidArgumentException
	 */
	public static function load($path) {
		$path = trim($path);
		if (!File::exists($path)) {
			throw new Exception($path . ' does not exist.');
		}
		$object = unserialize(File::get($path));
		if (!($object instanceof static)) {
			throw new InvalidArgumentException('Object is not valid.');
		}

		return $object;
	}

}
