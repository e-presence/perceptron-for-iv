<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
namespace EPresence\PerceptronForIv\Filesystem;

class File {
	/**
	 * @param string $filename
	 *
	 * @return bool
	 */
	public static function exists($filename) {

		return file_exists($filename);
	}

	/**
	 * @param string $filename
	 *
	 * @return mixed
	 */
	public static function get($filename) {

		return file_get_contents($filename);
	}

	/**
	 * @param string $filename
	 * @param mixed $data
	 * @param int $flags
	 *
	 * @return int
	 */
	public static function put($filename, $data, $flags = 0) {

		return file_put_contents($filename, $data, $flags);
	}

	/**
	 * @param string $filename
	 *
	 * @return bool
	 */
	public static function del($filename) {

		return static::unlink($filename);
	}

	/**
	 * @param string $filename
	 *
	 * @return bool
	 */
	public static function delete($filename) {

		return static::unlink($filename);
	}

	/**
	 * @param string $filename
	 *
	 * @return bool
	 */
	public static function unlink($filename) {

		return @unlink($filename);
	}

	/**
	 * @param string $filename
	 *
	 * @return bool
	 */
	public static function isWritable($filename) {

		return is_writeable($filename);
	}
}
