<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
require_once __DIR__ . '/../vendor/autoload.php';

use EPresence\PerceptronForIv\Perceptron\Perceptron;
use EPresence\PerceptronForIv\Perceptron\Trainer;
use EPresence\PerceptronForIv\Perceptron\TrainingData;

define('LOCSOLUNK', 1);
define('NEM_LOCSOLUNK', -1);

define('TALAJ_TELJESEN_SZARAZ', 0);
define('TALAJ_SZARAZ', 33);
define('TALAJ_NEDVES', 66);
define('TALAJ_TELJESEN_VIZES', 100);

/**
 * Calculates percent.
 *
 * @param number $a
 *
 * @return number
 */
function toPercent($a) {

	return round(($a + 1) / 2 * 100, 2);
}

$a_talaj_nedvessegtartalom_tartomany = array(0, 100); // %
$a_varhato_csapadek_mennyisege_tartomany = array(0, 300); // mm

$p = new Perceptron(2,
		array(
			$a_talaj_nedvessegtartalom_tartomany,
			$a_varhato_csapadek_mennyisege_tartomany,
		)
	);

$t = new Trainer($p);
$t->addTrainingData(new TrainingData(array(TALAJ_TELJESEN_SZARAZ, 0), LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_TELJESEN_SZARAZ, 1), LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_TELJESEN_SZARAZ, 20), NEM_LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_SZARAZ, 0), LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_SZARAZ, 1), LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_NEDVES, 0), NEM_LOCSOLUNK));
$t->addTrainingData(new TrainingData(array(TALAJ_TELJESEN_VIZES, 0), NEM_LOCSOLUNK));
$t->train();

$a_talaj_nedvessegtartalma = 23;
$a_varhato_csapadek_mennyisege = 0;

$locsoljunk_e = $p->process(array(
	$a_talaj_nedvessegtartalma,
	$a_varhato_csapadek_mennyisege,
));
echo toPercent($locsoljunk_e), '%', PHP_EOL;

$a_talaj_nedvessegtartalma = 43;
$a_varhato_csapadek_mennyisege = 1;

$locsoljunk_e = $p->process(array(
	$a_talaj_nedvessegtartalma,
	$a_varhato_csapadek_mennyisege,
));
echo toPercent($locsoljunk_e), '%', PHP_EOL;
