<?php
/**
 * @author Viktor Bliszkó <viktor.bliszko@e-presence.hu>
 * @copyright Copyright (c) 2016-2018, e-presence, http://e-presence.hu
 */
require_once __DIR__ . '/../vendor/autoload.php';

use EPresence\PerceptronForIv\Perceptron\Perceptron;
use EPresence\PerceptronForIv\Perceptron\Trainer;
use EPresence\PerceptronForIv\Perceptron\TrainingData;

$p = new Perceptron(2,
		array(
			array(0, 1),
			array(0, 1),
		)
	);

$t = new Trainer($p);
$t->addTrainingData(new TrainingData(array(0, 0), -1));
$t->addTrainingData(new TrainingData(array(1, 0), 1));
$t->addTrainingData(new TrainingData(array(0, 1), 1));
$t->addTrainingData(new TrainingData(array(1, 1), 1));

$t->train();

echo $p->process(array(0, 0)), PHP_EOL;
echo $p->process(array(1, 0)), PHP_EOL;
echo $p->process(array(0, 1)), PHP_EOL;
echo $p->process(array(1, 1)), PHP_EOL;
